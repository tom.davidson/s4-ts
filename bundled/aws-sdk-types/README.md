# Summary
This package contains type definitions for aws-sdk (https://github.com/aws/aws-sdk-js),
forked from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/types-2.0/aws-sdk
due to https://github.com/DefinitelyTyped/DefinitelyTyped/pull/11923
