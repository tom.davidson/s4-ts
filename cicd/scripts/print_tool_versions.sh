#!/bin/bash

printf "\nnpm:\n"; npm version;
printf "\nyarn:\n"; yarn --version;
printf "\nterraform:\n"; terraform --version;
printf "\naws:\n"; aws --version;
printf "\neb:\n"; eb --version;
