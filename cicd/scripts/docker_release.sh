#!/bin/bash
# usage: docker_release.sh image_tag repo:basetag version

SHORT_SHA1=$(git rev-parse --short --verify HEAD)
IMAGE_TAG=$1
REPO_TAG=$2
VERSION=$3

# basename $(dirname ../scripts/docker_release.sh )

docker pull $IMAGE_TAG

docker tag $IMAGE_TAG $REPO_TAG;
docker push $REPO_TAG

docker tag $IMAGE_TAG $REPO_TAG-$SHORT_SHA1;
docker push $REPO_TAG-$SHORT_SHA1

docker tag $IMAGE_TAG $REPO_TAG-lastest;
docker push $REPO_TAG-lastest

if [[ -v $VERSION ]]
then
  docker tag $IMAGE_TAG $REPO_TAG-$VERSION;
  docker push $REPO_TAG-$VERSION
fi