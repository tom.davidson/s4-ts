import {
  IConfig,
  IOptions,
  findReadFile,
  getParamsFromConfig,
  normalizeParams,
  searchForConfig,
} from "../../src/utils";
import * as chai from "chai";
import "core-js/modules/es6.promise";
import * as mocha from "mocha";

const expect = chai.expect;

describe("normalizeParams", () => {
  it("should parse s3 style urls", () => {
    let url = "s3://bucketname/key/name/";
    expect(normalizeParams(url)).to.have.property("Key", "key/name");
    expect(normalizeParams(url)).to.have.property("Bucket", "bucketname");
  });

  it("should *only* accept urls using s3 protocol", () => {
    let url = "https://domain.com/path/name/"
    expect(normalizeParams.bind(normalizeParams, url)).to.throw("S4 URLs must use the S3 protocol.");
  });

  it("should accept Object with Key and Bucket properties", () => {
    let p = < IOptions > {
      Bucket: "bucketname",
      Key: "key/name",
    };
    expect(normalizeParams(p)).to.have.property("Key", "key/name");
    expect(normalizeParams(p)).to.have.property("Bucket", "bucketname");
  });

});

describe("findReadFile", () => {
  it("should find test/package-single.json from ./test", () => {
    let res = <IConfig>{
      cwd: "./test",
      env: "null",
      file: "package-single.json",
      ns: "null",
    };
    return findReadFile(res).then((r: any) => {
      expect(r.data).to.have.property("name", "s4-test-single");
    })
  });

  it("should find s4/tsconfig.json from ./test", () => {
    let res = <IConfig> {
      cwd: "./test",
      env: "null",
      file: "tsconfig.json",
      ns: "null",
    };
    return findReadFile(res).then((r: any) => {
      expect(r).to.have.deep.property("data.compilerOptions");
    });
  });

  it("should find s4/package.json", () => {
    let res = < IConfig > {
      cwd: ".",
      env: "null",
      file: "package.json",
      ns: "null",
    };
    return findReadFile(res).then((r: any) => {
      expect(r).to.have.deep.property("data.name", "s4");
    });
  });

  it("should parse test/package-single.json", () => {
    let res = <IConfig>{
      cwd: __dirname + "/test",
      env: "production",
      file: "package-single.json",
      ns: "iac.s4",
    };
    return findReadFile(res).then((r: any) => {
      expect(r).to.include.keys(Object.keys(res));
      expect(r.data).to.include.keys("name", res.ns.split(".")[0], "version");
    });
  });
});

describe("searchForConfig", () => {
  it("should read 'iac' config in package.json", () => {
    let res = <IConfig> {
      env: "null",
      file: "package.json",
      ns: "iac.secrets",
    };
    return searchForConfig(res).then((r: any) => {
      expect(r).to.have.property("secrets");
      expect(r).to.not.have.property("s4");

    });
  });

  it("should find test/package-single.json", () => {
    let res = <IConfig> {
      cwd: "./test",
      env: "null",
      file: "package-single.json",
      ns: "iac.s4",
    };
    return searchForConfig(res).then((r: any) => {
      expect(r).to.have.deep.property("s4.default.file", "s4/test");
    });
  });

  it("should search up from ./test for tsconfig.json", () => {
    let res = <IConfig> {
      cwd: "./test",
      env: "null",
      file: "tsconfig.json",
      ns: "compilerOptions",
    };
    return searchForConfig(res).then((r: any) => {
      expect(r).to.have.deep.property("declaration");
    });
  });

  it("should error for namespace 'bob' in s4/tsconfig.json from ./test", () => {
    let res = <IConfig>{
      cwd:  "./test",
      env: "null",
      file: "tsconfig.json",
      ns: "bob",
    };
    return searchForConfig(res).catch((e: Error) => {
      expect(e.message).to.equal("CONFIG NOT FOUND");
    });
  });

  it("should error if file not found", () => {
    let res = <IConfig> {
      cwd: "./test",
      env: "null",
      file: "asdjfalsdfa.json",
      ns: "bob",
    };
    return searchForConfig(res).catch((e: Error) => {
      expect(e.message).to.equal("CONFIG NOT FOUND");
    });
  });

});

describe("getParamsFromConfig", () => {
  it("should parse test/package-envs.json for env 'production'", () => {
    let res = <IConfig>{
      cwd: "./test",
      env: "production",
      file: "package-envs.json",
      ns: "iac.secrets",
    };

    return getParamsFromConfig(res).then((r: any) => {
      expect(r).to.have.property("file", "../secret-prod.json");
    });
  });


  it("should parse test/package-single.json", () => {
    let res = <IConfig>{
      cwd: "./test",
      env: "default",
      file: "package-single.json",
      ns: "iac.s4",
    };
    return getParamsFromConfig(res).then((r: any) => {
      expect(r).to.have.property("file", "s4/test");
    });
  });

  it("should error with wrong env for test/package-envs.json", () => {
    let res = <IConfig>{
      cwd: "./test",
      env: "wrong",
      file: "package-envs.json",
      ns: "iac.secrets",
    };
    return getParamsFromConfig(res).catch((e: Error) => {
        expect(e.message).to.equal("ENVIRONMENT NOT FOUND");
    });
  });
});
