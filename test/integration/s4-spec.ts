import {default as S4} from "../../src/s4";
import * as AWS from "aws-sdk";
import { exec, env } from "shelljs";
import * as chai from "chai";
import * as mocha from "mocha";
import "core-js/modules/es6.promise";
const expect = chai.expect;
const testBucketName = "s4-test-bucket-5";

describe("s4 integration:", () => {
  const s3 = new AWS.S3({
    apiVersion: "2006-03-01"
  });

  before(function (done) {
    const bucketParams = {
      Bucket: testBucketName
    };
    s3.createBucket(bucketParams, function (err, data) {
      if (err) {
        done(err);
      } else {
        console.log("S3 Bucket is setup: ", data.Location);
        done();
      }
    });
  })

  after(function (done) {
    console.log('Teardown - destroy S3 Bucket')
    const bucketParams = {
      Bucket: testBucketName
    };
    return s3.deleteBucket(bucketParams, function (err, data) {
      if (err) {
        done(err);
      } else {
        console.log("S3 Bucket is torn down: ", data)
        done();
      }
    });
  })

  describe("cli:", () => {
    it("should put test file ", () => {
      //npm_package_version
      env["npm_lifecycle_event"] = "deploy:test:production";

      exec('echo hi');
      // return searchForConfig(res).catch((e: Error) => {
      //   expect(e.message).to.equal("CONFIG NOT FOUND");
      // });
    });

    // it("s4.get the correct secret file from the real bucket", () => {
    //   expect(false).to.true;
    // });
  })

  describe("api:", () => {
    it("should put test file ", () => {
      //npm_package_version
      env["npm_lifecycle_event"] = "deploy:test:production";

      exec('echo hi');
      // return searchForConfig(res).catch((e: Error) => {
      //   expect(e.message).to.equal("CONFIG NOT FOUND");
      // });
    });

    // it("s4.get the correct secret file from the real bucket", () => {
    //   expect(false).to.true;
    // });
  })

});
