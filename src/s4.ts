import {
  IConfig,
  IOptions,
  getParamsFromConfig,
  normalizeParams,
} from "./utils";
import * as AWS from "aws-sdk";
import "core-js/modules/es6.object.assign";
import "core-js/modules/es6.promise";

AWS.config.apiVersions = {
  s3: "2006-03-01",
};

export default class S4 {

  public static get(params?: IOptions | String) {
    if (params === undefined) {
      getParamsFromConfig({
      env: process.env.NODE_ENV,
      file: "package.json",
      ns: "iac.secrets",
    })
        .then((p: IConfig) => {
          params = Object.assign(p);
        });
    }
    const s3 = new AWS.S3();
    const options = normalizeParams(params);
    return s3.getObject(options).promise();
  }

  public static put(params?: IOptions | String) {
    if (params === undefined) {
      getParamsFromConfig({
          env: process.env.NODE_ENV,
          file: "package.json",
          ns: "iac.secrets",
        })
        .then((p: IConfig) => {
          params = Object.assign(p);
        });
    }

    const s3 = new AWS.S3();
    const defaults = {
      ACL: "private",
      ServerSideEncryption: "AES256",
    };
    const options = normalizeParams(params, defaults);

    return s3.putObject(options).promise();
  }
}
