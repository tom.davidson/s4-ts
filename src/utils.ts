import "core-js/modules/es6.object.assign";
import "core-js/modules/es6.promise";
import * as fs from "fs";
import * as path from "path";
import * as url from "url";

export interface IOptions {
  Body?: string;
  Bucket: string;
  Key: string;
  [key: string]: string;
}

function isIOptions(object: any): object is IOptions {
    return "Key" in object && "Bucket" in object;
}

export interface IConfig {
  cwd?: string;
  env: string;
  file: string;
  ns: string;
  data?: any;
  [key: string]: string | JSON;
}

export function normalizeParams(params: Object = {}, defaults: Object = {}): IOptions {
  let parsed = <IOptions> {};
  if (typeof params === "string") {
    let parsedURL = url.parse(params);
    if (parsedURL.protocol.includes("s3") !== true) {
      throw new Error("S4 URLs must use the S3 protocol.");
    }
    parsed.Key = parsedURL.pathname.replace(/^\/|\/$/g, "");
    parsed.Bucket = parsedURL.hostname;

  } else if (isIOptions(params)) {
    parsed = Object.assign(parsed, params);
  } else {
    throw new Error("UNSUPPORTED PARAMS TYPE");
  }
  return Object.assign(parsed, defaults);
}

export function getParamsFromConfig(params: IConfig): Promise<any> {
  return searchForConfig(params)
    .then((currentConfig: any) => {
      return Promise.resolve(currentConfig[params.ns.split(".")[1]][params.env]);
    })
    .catch((e: Error) => {
      Promise.reject(Error("ENVIRONMENT NOT FOUND"));
    });
}

export function searchForConfig(fileInfo: IConfig): Promise<any> {
  return findReadFile(fileInfo)
    .then((currentFileInfo: IConfig) => {
      const isFound = !!currentFileInfo.data[currentFileInfo.ns.split(".")[0]];

      if (isFound) {
        return Promise.resolve(currentFileInfo.data[currentFileInfo.ns.split(".")[0]]);
      } else {
        currentFileInfo.cwd = path.resolve(currentFileInfo.cwd, "..");
        return searchForConfig(currentFileInfo);
      }
    })
    .catch((e: Error) => {
      return Promise.reject(Error("CONFIG NOT FOUND"));
    });
}

export function findReadFile(fileInfo: IConfig): Promise<any> {
  fileInfo.cwd = path.resolve(fileInfo.cwd || ".");
  const ext = path.parse(fileInfo.file).ext;
  const root = path.parse(fileInfo.cwd).root;

  if (ext !== ".json") {
    return Promise.reject(Error("UNSUPPORTED FILETYPE"));
  }

  if (fileInfo.cwd === root) {
    return Promise.reject(Error("ENOENT"));
  }

  return new Promise((resolve, reject) => {
    fs.readFile(path.resolve(fileInfo.cwd, fileInfo.file), "utf8", (err, data) => {
      const newFileInfo = Object.assign(<IConfig> {}, fileInfo);

      if (err) {
        if (err.code === "ENOENT") {
          newFileInfo.cwd = path.resolve(fileInfo.cwd, "..");
          resolve(findReadFile(newFileInfo));
        } else {
          reject(err);
        }
      } else {
        if (ext === ".json") {
          newFileInfo.data = JSON.parse(data);
        }
        resolve(newFileInfo);
      }
    });
  });
}
