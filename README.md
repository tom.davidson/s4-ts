# Secret Simple Secure Storage - S4

S4 implements a basic S3 encryption client for NodeJS and CLI for deploying secrets. Its just a simple [AWS SDK][] wrapper that enforces SSE and versioning. It would be insignificant to transparently use KMS for client-side encryption as well, but client side encryption seems unnecessary given that SSE forces SSL for transport.

## Usage

- CLI uses NPM lifecycle events that must match the same namespace in the config.
- CLI does not have many options and instead reads config from package.json
- Secrets are immutable so the CLI does not get, only put.
- The S3 bucket keys names are appended with the package.json version.
- s4.get() also matches the versioning from package.json.

```js
// package.json & cli

"scripts": {
    "deploy:stage": "s4 && deploy scripts",
    "deploy:production": "s4 && deploy scripts",
},
 "iac": {
    "secrets": {
      "production": {
        "file": "../secret-prod.json",
        "target": "s3://bucketname/config"
      },
      "stage": {
        "file": "../secret-stage.json",
        "target": "s3://bucketname/config"
      }
    }
  }

// or

"iac": {
    "s4": {
      "default": {
        "file": "s4/test",
        "remote-file": "s3://s4-ops/secrets/secrets.json"
      }
    }
  }

// api

import { s4 } from 's4';

s4.get(process.env.NODE_ENV);

```

## Contributing

### Potential features

1. Alternate config file and namespace
1. [KMS encryption envelope workflow][eew] such as the [Java S3 encryption client][jec].
1. as a SystemJS plugin
1. cli arguments
1. deps reduction


```

[jec]: http://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc/com/amazonaws/services/s3/AmazonS3EncryptionClient.html
[eew]: http://docs.aws.amazon.com/kms/latest/developerguide/workflow.html
[AWS SDK]: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS.html
